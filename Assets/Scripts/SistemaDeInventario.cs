using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;

public class SistemaDeInventario : MonoBehaviour
{

    public const string KeyCatalog = "CatalogoDeAnillos";

    public List<BotonItem> BotonItems = new List<BotonItem>();

    private Dictionary<string, BotonItem> _dictionaryBotonItems = new Dictionary<string, BotonItem>();

    public Text TextoAnillos;
    public Text TextoPrecioObjetoActual;
    private int Anillos;

    public Button BotonComprar;

    public Text TextoNombre;
    public Text TextoDescripcion;
    public Image ImageInformacion;

    public Animator animatorInformacion;
    
    void Start()
    {
        foreach (var item in BotonItems)
        {
            _dictionaryBotonItems.Add(item.KeyItem,item);
        }
        CrearInventory();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CrearInventory()
    {
        var request = new GetCatalogItemsRequest()
        {
            CatalogVersion = KeyCatalog
        };
        PlayFabClientAPI.GetCatalogItems(request,obtencionDeCatalogo,OnError);
    }

    public void OnError(PlayFabError error)
    {
        Debug.LogError(error.ErrorMessage);
    }

    public void obtencionDeCatalogo(GetCatalogItemsResult result)
    {
        foreach (var item in result.Catalog)
        {
            if (_dictionaryBotonItems.ContainsKey(item.ItemId))
            {
                _dictionaryBotonItems[item.ItemId].boton.interactable = true;
                
                //Un combercion hace que el uint no identificados (int) lo hara un int 
                _dictionaryBotonItems[item.ItemId].SetValues((int)item.VirtualCurrencyPrices[SistemaDeEconomia.KeyCurrencies], this, item.Description);
            }
        }

        var request = new GetUserInventoryRequest();
        
        PlayFabClientAPI.GetUserInventory(request,ObtencionExitosa,OnError);

    }


    public void ObtencionExitosa(GetUserInventoryResult result)
    {
        Anillos = result.VirtualCurrency[SistemaDeEconomia.KeyCurrencies];
        ActualizarUI();
    }

    public void ActualizarUI()
    {
        
        TextoAnillos.text = "Anillos : " + Anillos.ToString();
    }

    public void MostrarObjetoActual(int Precio, bool usaImagen, string Nombre, string descripcion, Vector2Int ValoresAnimacion)
    {
        TextoPrecioObjetoActual.text = "Costo : " + Precio.ToString();

        TextoNombre.text = Nombre;
        TextoDescripcion.text = descripcion;
        
        if (Anillos >= Precio)
        {
            BotonComprar.interactable = true;
            
        }
        else
        {
            BotonComprar.interactable = false;
        }

        if (usaImagen == true)
        {
            ImageInformacion.gameObject.SetActive(true);
            TextoDescripcion.gameObject.SetActive(false);
            animatorInformacion.SetBool("AnimacionActiva", true);
        }
        else
        {
            ImageInformacion.gameObject.SetActive(false);
            TextoDescripcion.gameObject.SetActive(true);
            animatorInformacion.SetBool("AnimacionActiva", false);
        }

        animatorInformacion.SetFloat("X", ValoresAnimacion.x); 
        animatorInformacion.SetFloat("Y", ValoresAnimacion.y);
    }
    
}
