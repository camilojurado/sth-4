﻿using UnityEngine;

/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T _instance;
	
	public static T Instance
	{
		get
		{
            if (_instance == null)
            {
#if UNITY_EDITOR
                _instance = (T) FindObjectOfType(typeof(T));
                if(_instance != null)
                    return _instance;
#endif
                Debug.LogError("Singleton not on scene " + typeof(T).ToString());
                return null;
            }
            return _instance;
        }
	}

    void Awake()
    {
        _instance = this.GetComponent<T>();
        onAwake();
    }

    protected virtual void onAwake()
    {
		Debug.Log("awake singleton");
		
    }

	public void OnDestroy () 
	{
		//applicationIsQuitting = true;
		_instance = null;
	}
}