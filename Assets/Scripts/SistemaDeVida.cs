using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Animations;
using UnityEngine.UI;

public class SistemaDeVida : MonoBehaviour
{
    public string nombreEscena;
    
    public Text TextoVidas;
    
    public int NumVidas = 3;

    public GameObject PuntoDeResureccion;
    
    Animator AnimationComp;

    public AudioClip golpeado;
    
    void Start()
    {
        AnimationComp = GetComponent<Animator>();
        ActualizarTextoVidas();
    }

    
    void Update()
    {
        
    }

    public void DisminuirVida()
    {
        Dañado();
        if (Puntos.instance.QuitarAnillos() == false)
        {
            NumVidas--;
            transform.position = PuntoDeResureccion.transform.position;
            
        }
        else
        {
            SoundManager.PlaySfx(golpeado);
        }
        ActualizarTextoVidas();
        if (NumVidas == 0)
        {
            
            CambiarEscenaPorNombre(nombreEscena);
        }
        
    }

    public void ActualizarTextoVidas()
    {
        TextoVidas.text = NumVidas.ToString();
    }

    public void Dañado()
    {
        AnimationComp.SetTrigger("Golpeado");
    }
    
    

    public void CambiarEscenaPorNombre(string nombreEscena)
    {
        SceneManager.LoadScene(nombreEscena);
    }


    public void OnCollisionEnter2D(Collision2D colicion)
    {
        if (colicion.collider.CompareTag("Enemigo"))
        {
            DisminuirVida();
            
        }
        
        if (colicion.collider.CompareTag("Proyec"))
        {
            DisminuirVida();
            
        }
            
        
    }
    
}
