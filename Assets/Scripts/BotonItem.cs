using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonItem : MonoBehaviour
{

    public int precioDelObjeto;
    
    public string KeyItem;

    public Button boton;

    private SistemaDeInventario _sistemaDeInventario;

    public bool UsaImagen;

    public string Descripcion;

    public string NombreDeDescripcion;

    public Vector2Int ValorAnimator;

    private bool EsdePLayFab = false;
    
    public void SetValues(int Valor, SistemaDeInventario sistemaDeInventario ,string descripcion)
    {
        precioDelObjeto = Valor;
        _sistemaDeInventario = sistemaDeInventario;
        Descripcion = descripcion;
        NombreDeDescripcion = KeyItem;

        EsdePLayFab = true;

    }
    
    

    public void SeleccionarObjeto()
    {
        if (_sistemaDeInventario == null)
        {
            _sistemaDeInventario = FindObjectOfType<SistemaDeInventario>();
        }
        _sistemaDeInventario.MostrarObjetoActual(precioDelObjeto, UsaImagen, NombreDeDescripcion, Descripcion, ValorAnimator);
    }

}
