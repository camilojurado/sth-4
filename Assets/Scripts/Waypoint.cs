using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    [SerializeField] private GameObject[] _waypoint;
    [SerializeField] private float _speed;
    [SerializeField] private float _minDistance;
    [SerializeField] private int _index;
    [SerializeField] private bool _isPlatform;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, _waypoint[_index].transform.position);
        if (distance < _minDistance)
        {
            if (_index == _waypoint.Length - 1)
            {
                _index = 0;
            }
            else
            {
                _index++;
            }
        }

        transform.position = Vector3.MoveTowards(transform.position, _waypoint[_index].transform.position,
            _speed * Time.deltaTime);
        

    }

    void OnCollisionEnter2D(Collision2D Ob)
    {
        if (Ob.gameObject.CompareTag("Player") && _isPlatform)
        {
            Ob.gameObject.transform.parent = gameObject.transform;
        }
    }


    void OnCollisionExit2D(Collision2D Ob)
    {
        if (Ob.gameObject.CompareTag("Player") && _isPlatform)
        {
            Ob.gameObject.transform.parent = null;
        }
    }
}
