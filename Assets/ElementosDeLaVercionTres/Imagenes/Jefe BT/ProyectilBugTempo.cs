﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilBugTempo : MonoBehaviour
{
    
    
    Transform player;

    //[SerializeField]
    //float argoRange;

    //[SerializeField]
    //float moveSpeed;

    [SerializeField] 
    int Duracion;
    
    Rigidbody2D rb2d;

    
    
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        StartCoroutine("TiempoProBug");
        player = FindObjectOfType<SilverControles>().GetComponent<Transform>();
        
    }

    // Update is called once per frame
    
    /*
    void Update()
    {
        float distToPlayer = Vector2.Distance(transform.position, player.position);

        if (distToPlayer < argoRange)
        {
            ChasePlayer();
        }
    }
    
    void ChasePlayer()
    {

        Vector2 direccion = player.position - transform.position;
        rb2d.velocity = direccion.normalized * moveSpeed;

        if (transform.position.x < player.position.x)
        {
            
            transform.localScale = new Vector2(0.5f, 0.5f);

        }
        else
        {
           
            transform.localScale = new Vector2(-0.5f, 0.5f);
        }


    }
    */
    IEnumerator TiempoProBug()
    {
        yield return new WaitForSeconds(Duracion);
        Destroy(gameObject);
    }
    
        
    
    
}
