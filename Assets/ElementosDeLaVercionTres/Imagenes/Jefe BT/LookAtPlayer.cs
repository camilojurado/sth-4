﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{

    private Transform player;

    [SerializeField]
    private float rotSpeed = 5f;


    public GameObject Proyectil;
    public float velProyectil;

    public float RangoAtaque;

    public GameObject PuntoAtaque;

    public float retardo;
    private float RetardoAux;
    
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<SilverControles>().GetComponent<Transform>();
        RetardoAux = retardo;
    }

    // Update is called once per frame
    void Update()
    {
        float distToPlayer = Vector2.Distance(transform.position, player.position);

        if (distToPlayer < RangoAtaque)
        {
            retardo -= Time.deltaTime;
            if (retardo <= 0)
            {
                Shot();
                retardo = RetardoAux;
            }
            
        }
        
        Vector2 direccion = player.position - transform.position;
        float angle = Mathf.Atan2(direccion.x, direccion.y) * Mathf.Rad2Deg;
        Quaternion rotacion = Quaternion.AngleAxis(angle -120,  Vector3.back);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotacion, rotSpeed * Time.deltaTime);
    }

    void Shot()
    {
        Vector2 direccion = player.position - transform.position;
        GameObject proyectilBoss = Instantiate(Proyectil,PuntoAtaque.transform.position, Quaternion.identity);
        proyectilBoss.GetComponent<Rigidbody2D>().AddForce(direccion * velProyectil, ForceMode2D.Impulse);
    }

    
}
