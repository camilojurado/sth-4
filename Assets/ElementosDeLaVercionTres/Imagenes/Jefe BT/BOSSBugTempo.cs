﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BOSSBugTempo : MonoBehaviour
{
    public Transform[] target;
    public float speed = 6.0f;

    int curPos = 0;
    int nextPos = 1;


    private bool moveNext = true;
    public float timeToNext = 2.0f;


    [SerializeField] 
    public int VideJefe;


    [SerializeField] 
    private int xFin = 0;

    [SerializeField] 
    private int yFin = 0;
    
    [SerializeField] 
    private int zFin = 5;
    
    [SerializeField] 
    public int VelFin = 10;
    
    private void FixedUpdate()
    {
        if (moveNext)
        {
            transform.position = Vector2.MoveTowards(transform.position, target[nextPos].position, speed * Time.deltaTime);
        }    
        

        if(Vector2.Distance(transform.position, target[nextPos].position) <= 1)
        {
            StartCoroutine(TimeMove());
            curPos = nextPos;
            nextPos++;


            if (nextPos > target.Length - 1)
                nextPos = 0;
           

            

        }

    }

    void Update()
    {
        if (VideJefe <= 0)
        {
            StartCoroutine("Derrotado");
            
        }
    }
    
    
    private void OnTriggerEnter2D(Collider2D Colicion)
    {
        if (Colicion.CompareTag("OndaPES"))
        {
            VideJefe -= 1;
        }
    }

    IEnumerator TimeMove()
    {
        yield return new WaitForSeconds(timeToNext);
    }

    IEnumerator Derrotado()
    {
        
        transform.Rotate(xFin * Time.deltaTime * VelFin,yFin * Time.deltaTime * VelFin ,zFin * Time.deltaTime * VelFin);
        
        yield return new WaitForSeconds(2f);
        
        
        
        Destroy(gameObject);
    }
    
}
