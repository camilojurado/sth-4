﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Transparencia : MonoBehaviour
{
    [Range(0, 1)] 
    public float LaTransparencia = 0, transsitionSpeed = 1;

    public SpriteRenderer spriteRenderer;

    bool canButton = true;

    public enum Modo
    {
        Show = 0,
        Hide = 1,
        Nothing = -1,

    };

    public Modo modo;
    
    
    // Start is called before the first frame update
    void Start()
    {
        modo = Modo.Nothing;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            modo = Modo.Show;
        }

        if (Input.GetKeyUp(KeyCode.B))
        {
            modo = Modo.Hide;
        }

        if (modo.Equals(Modo.Hide))
        {
            if (LaTransparencia <= 0)
                modo = Modo.Nothing;

            LaTransparencia -= Time.deltaTime;
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b,
                LaTransparencia);
        }

        if (modo.Equals(Modo.Show))
        {
            if (LaTransparencia >= 1)
                modo = Modo.Nothing;


            LaTransparencia += Time.deltaTime / 2;
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b,
                LaTransparencia);
        }
        
    }

    public void Activate()
    {
        canButton = true;
        
        //GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
        //Color.Lerp(new Color(1, 1, 1, 1), new Color(1, 1, 1, 0), 2);

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemigo"))
        {
            Destroy(other.gameObject);
            
        }
    }
}
