﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ONDAAcenso : MonoBehaviour
{
    
    Vector2 Direccion;

    [SerializeField]

    int Velocidad = 10;


    [SerializeField]

    int Tiempo = 1;

    
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("TiempoDeOnda");
        Direccion = Vector2.up;
        
        transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Translate(Direccion * Velocidad * Time.deltaTime);
    }
    
    
    IEnumerator TiempoDeOnda()
    {
        yield return new WaitForSeconds(Tiempo);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemigo"))
        {
            Destroy(other.gameObject);
            
        }
    }
}
