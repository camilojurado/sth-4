﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PESdeAcenso : MonoBehaviour
{
    [SerializeField]

    Transform origenOnda;

    [SerializeField] 
    
    Transform OriOndaEste;
    
    [SerializeField] 
    
    Transform OriOndaOeste;
    
    [SerializeField]

    GameObject OndaTELE;
    
    public AudioClip SoAcenso;
    
    void Start()
    {
        
    }

    
    void Update()
    {
        if (Input.GetKeyDown("h"))
        {
            SoundManager.PlaySfx(SoAcenso);
            GameObject Wave = Instantiate(OndaTELE, origenOnda.transform.position, origenOnda.transform.rotation, transform);
            GameObject WaveE = Instantiate(OndaTELE, OriOndaEste.transform.position, OriOndaEste.transform.rotation, transform);
            GameObject WaveO = Instantiate(OndaTELE, OriOndaOeste.transform.position, OriOndaOeste.transform.rotation, transform);
        }
    }
}
