﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cargando : MonoBehaviour
{

    public string NivelGame;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Escenario");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Escenario()
    {
        yield return new WaitForSeconds(7);
        SceneManager.LoadScene(NivelGame);

    }
}
