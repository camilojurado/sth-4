﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalidaDelJefe : MonoBehaviour
{
    [SerializeField] 
    private GameObject Jefe;


    private BoxCollider2D Colicion; 
        
        
    // Start is called before the first frame update
    void Start()
    {
        Colicion = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Jefe == null)
        {
            StartCoroutine("Abrir");
        }
    }


    IEnumerator Abrir()
    {
        yield return new WaitForSeconds(2);
        Colicion.isTrigger = true;
    }
}
