﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;



public class SilverControles : MonoBehaviour
{

    public LayerMask Piso;
    public bool EnelAire = false;

    public BarraDePES PES;

    Animator AnimacionComp;


    [SerializeField]

    float Vel = 3.0f;

    /*
    [SerializeField]

    float Salto = 3.0f;
    */

    [SerializeField]

    float Jump = 10f;


    [SerializeField]

    float DashSpeed = 10f;


    public AudioClip SoSalto;
    public AudioClip SoImpulso;


    // Start is called before the first frame update
    void Start()
    {
        AnimacionComp = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Correr();
        Saltando();
        ATKbasico();
        Coliciones();
        Flote();
        TeleObjeto();
        TeleExplocion();
    }

    public void Correr()
    {
        float Horizontal = Input.GetAxis("Horizontal");

        if (Horizontal != 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = Horizontal < 0;
            transform.Translate(Vector2.right * Time.deltaTime * Vel * Horizontal);
            //AnimacionComp.SetTrigger("Movimiento");
            AnimacionComp.SetBool("MOVI", true);
        }
        else
        {
            //AnimacionComp.ResetTrigger("Movimiento");
            AnimacionComp.SetBool("MOVI", false);
        }
        
        //silver.Corriendo(Horizontal != 0);

    }

    public void Saltando()
    {

        if (Input.GetKeyDown("space") && Coliciones())
        {
            SoundManager.PlaySfx(SoSalto);
            //silver.Saltando(true);
            gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * Jump, ForceMode2D.Impulse);
            //rigidBody2D.AddForce(Vector2.up * Jump, ForceMode2D.Impulse);
            AnimacionComp.SetTrigger("ElSalto");
            AnimacionComp.SetBool("SilverCayendo", true);
            

        }
        else
        {
            AnimacionComp.ResetTrigger("ElSalto");
            AnimacionComp.SetBool("SilverCayendo", false);
        }
       

    }

    public void ATKbasico ()
    {
        if (Input.GetKeyDown("g")&& !PES.GetPesActual() )
        {
            AnimacionComp.SetTrigger("AtaqueBasico");
            PES.RedusPESActual(1);
        }
        else if (Input.GetKeyUp("g"))
        {
            PES.ResetPESActual();
        }
    }

    public void Flote ()
    {
        float Horizontal = Input.GetAxis("Horizontal");

        if (Input.GetKeyDown("f") && !Coliciones() && !PES.GetPesActual())
        {
            SoundManager.PlaySfx(SoImpulso);
            AnimacionComp.SetTrigger("SilverImpulso");
            Vector2 dir = GetComponent<SpriteRenderer>().flipX ? -transform.right : transform.right;
            gameObject.GetComponent<Rigidbody2D>().AddForce(dir * 80, ForceMode2D.Impulse);
            PES.RedusPESActual(2);
            
            
        }
        else if (Input.GetKeyUp("f"))
        {
            PES.ResetPESActual();
        }
        
    }

    public void TeleObjeto ()
    {
        if (Input.GetKeyDown("h") && !PES.GetPesActual())
        {
            AnimacionComp.SetTrigger("Tele");
            
            
            
            PES.RedusPESActual(5);
            
        }
        else if (Input.GetKeyUp("h"))
        {
            PES.ResetPESActual();
        }
        
    }

    public void TeleExplocion ()
    {
        if (Input.GetKeyDown("b") && !PES.GetPesActual())
        {
            AnimacionComp.SetTrigger("TeleExplocion");
            
            
            PES.RedusPESActual(8);
            
        }
        else if (Input.GetKeyUp("b"))
        {
            PES.ResetPESActual();
        }
        
    }




    public bool Coliciones()
    {

        if (Physics2D.Raycast(transform.position, Vector2.down, 2f, Piso.value))
        {
            AnimacionComp.SetBool("SilverParado", true);
            EnelAire = false;
            return true;

        }
        else
        {
            AnimacionComp.SetBool("SilverParado", false);
            EnelAire = true;
            return false;
        }
    }
}
