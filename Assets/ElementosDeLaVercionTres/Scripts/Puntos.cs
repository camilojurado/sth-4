﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntos : MonoBehaviour
{

    //Static es para que se pueda acceder a ella sin tener que hacer referencia en si
    public static Puntos instance;
    
    public int ValPunTos = 0;
    public int ValAnillos = 0;
    public int ValEnemigo = 0;

    
    public Text PunTos;
    public Text Anillos;

    void Awake()
    {
        instance = this;
    }
    
    void Start()
    {
        
        
    }

    
    void Update()
    {
        PunTos.text =  ValPunTos.ToString("0000000");
        Anillos.text = ValAnillos.ToString();
    }


    public bool QuitarAnillos()
    {
        if (ValAnillos > 0)
        {
            //Si es que tubiera anillos no le quitaria un pinto de vida
            ValAnillos = 0;
            return true;
        }

        return false;
    }
}
