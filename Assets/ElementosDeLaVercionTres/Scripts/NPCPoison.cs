﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCPoison : MonoBehaviour
{
    [SerializeField]
    Transform player;

    [SerializeField]
    float argoRange;

    [SerializeField]
    float moveSpeed;

    Rigidbody2D rb2d;


    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float distToPlayer = Vector2.Distance(transform.position, player.position);

        if (distToPlayer < argoRange)
        {
            ChasePlayer();
        }
        else
        {
            StopChasingPlayer();
        }
    }

    void ChasePlayer()
    {

        Vector2 direccion = player.position - transform.position;
        rb2d.velocity = direccion.normalized * moveSpeed;

        if (transform.position.x < player.position.x)
        {

            transform.localScale = new Vector2(0.2f, 0.2f);

        }
        else
        {

            transform.localScale = new Vector2(0.2f, 0.2f);
        }


    }

    void StopChasingPlayer()
    {
        rb2d.velocity = new Vector2(0, 0);
    }
}
