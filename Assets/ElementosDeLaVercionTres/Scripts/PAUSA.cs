﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PAUSA : MonoBehaviour
{

    private bool juegoPausado = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (juegoPausado)
            {
                Reanudar();
            }
            else
            {
                Pausa();
            }
        }
    }

    //[SerializeField] 
    //private GameObject LaPausa;


    [SerializeField] 
    private GameObject menuPausa;

    public void Pausa()
    {
        SwichMetods(false);
        SoundManager.MusicPausa(true);
        juegoPausado = true;
        Time.timeScale = 0f;
        //LaPausa.SetActive(false);
        menuPausa.SetActive(true);
    }

    public void SwichMetods(bool state) 
    {
        SwitchStatePesTactil(state);
        SwichStateAtaqueOnda(state);
        SwichStateImpulsoPes(state);
        SwichStateEscudoPES(state);
        SwichStateSalto(state);
        SwichStateMove(state);

    }

    void SwitchStatePesTactil(bool state)
    {
        foreach (var pes in FindObjectsOfType<PES_Tactil>())
        {
            pes.enabled = state;
        }

        
    }

    void SwichStateAtaqueOnda(bool state) 
    {
        foreach (var hit in FindObjectsOfType<AtaqueOnda>())
        {
            hit.enabled = state;
        }
    }

    void SwichStateEscudoPES(bool state) 
    {
        foreach (var pes in FindObjectsOfType<EscudoPES>()) 
        {
            pes.enabled = state;
        }
    }

    void SwichStateImpulsoPes(bool state) 
    {
        foreach (var pes in FindObjectsOfType<ImpulsoPES>()) 
        {
            pes.enabled = state;
        }
    }

    void SwichStateSalto(bool state) 
    {
        foreach (var jump in FindObjectsOfType<SaltoV>()) 
        {
            jump.enabled = state;
        }

    }

    void SwichStateMove(bool state) 
    {

        foreach (var move in FindObjectsOfType<MovimientoH>()) 
        {
            move.enabled = state;
        }
    }



    public void Reanudar()
    {
        StartCoroutine("TiempoDeRetorno");
       // SoundManager.MusicPausa(false);
       // juegoPausado = false;
       // Time.timeScale = 1f;
       //// LaPausa.SetActive(true);
       // menuPausa.SetActive(false);
    }

    /*
    public void Reiniciar()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

    public void Cerrar()
    {
        Application.Quit();
    }
    */


    IEnumerator TiempoDeRetorno() 
    {
        Time.timeScale = 1f;
        yield return new WaitForSeconds(.1f);
        SwichMetods(true);
        SoundManager.MusicPausa(false);
        juegoPausado = false;
        
        // LaPausa.SetActive(true);
        menuPausa.SetActive(false);
    }
    
}
