﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertasDelJefe : MonoBehaviour
{
    private BoxCollider2D Colicion;

    private GameObject player;
    
    // Start is called before the first frame update
    void Start()
    {
        Colicion = GetComponent<BoxCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
        Colicion.isTrigger = !(player.transform.position.x > transform.position.x &&
                               (player.transform.position.x - transform.position.x) < 5);
    }

    /*
     private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine("Cierre");
        }
    }


    IEnumerator Cierre()
    {
        yield return new WaitForSeconds(2);
        Colicion.isTrigger = false;
    }
    */
}
