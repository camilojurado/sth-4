﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDePES : MonoBehaviour
{
    public Image barraPes;

    public float PesActual;

    public float PesMaxima;

    public bool GetPesActual()
    {
        return PesActual <= 0;
    }

    public void ResetPESActual()
    {
        PesActual = PesMaxima;
    }
    
    public void RedusPESActual(float montoPES)
    {
        PesActual -= montoPES;
    }
    void Update()
    {
        barraPes.fillAmount = PesActual / PesMaxima;

        if (PesActual == 0)
        {
            
        }
    }
}
