﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBloodPlant : MonoBehaviour
{
    
    [SerializeField]
    Transform player;

    [SerializeField]
    float argoRange;
    
    //Rigidbody2D rb2d;
    
    Animator AnimacionNPC;

    private Vector3 localScale;

    public float RangoDeAtaque;



    public AudioClip SoDestru;
    private Puntos ManagerPuntos;
    public int Puntos;
    void Start()
    {
        //rb2d = GetComponent<Rigidbody2D>();
        AnimacionNPC = GetComponent<Animator>();
        AnimacionNPC.enabled = false;
        localScale = transform.localScale;
        ManagerPuntos = FindObjectOfType<Puntos>();
    }

    
    void Update()
    {
        float distToPlayer = Vector2.Distance(transform.position, player.position);

        if (distToPlayer < argoRange)
        {
            AnimacionNPC.enabled = true;
            Vector2 dir = player.position - transform.position;
            //GetComponent<SpriteRenderer>().flipX = dir.x < 0;
            transform.localScale = dir.x < 0 ? localScale :new Vector3(-localScale.x, localScale.y, localScale.z);
            

            AnimacionNPC.SetBool("BPEscondido", false);
        }
        else
        {
            //Esconderse
            AnimacionNPC.SetBool("BPEscondido", true);
        }

        if (distToPlayer < RangoDeAtaque)
        {
           AnimacionNPC.SetBool("BPAtacando", true);
        }
        else
        {
            AnimacionNPC.SetBool("BPAtacando", false);
        }
        
        
    }
    
    private void OnTriggerEnter2D(Collider2D Colicion)
    {
        if (Colicion.CompareTag("OndaPES"))
        {
            SoundManager.PlaySfx(SoDestru);
            ManagerPuntos.ValPunTos += Puntos;
            ManagerPuntos.ValEnemigo++;
            
        }
    }
    
    
}
