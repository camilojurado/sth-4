﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Anillos : MonoBehaviour
{
    public AudioClip Recoleccion;
    private Puntos ManagerPuntos;
    public int Puntos;
    

    
    
    void Start()
    {
        ManagerPuntos = FindObjectOfType<Puntos>();
        
    }

    
    void Update()
    {
        
    }

    private void OnTriggerEnter2D (Collider2D Colicion)
    {
        if (Colicion.CompareTag("Player"))
        {
            SoundManager.PlaySfx(Recoleccion);
            
            ManagerPuntos.ValPunTos += Puntos;
            ManagerPuntos.ValAnillos++;
            Destroy(gameObject);

            //PlayerPrefs.SetInt("Anillos", ManagerPuntos.ValPunTos);
            
            
        }



    }


}
