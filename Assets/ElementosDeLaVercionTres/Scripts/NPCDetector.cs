﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDetector : MonoBehaviour
{

    [SerializeField]
    Transform player;

    [SerializeField]
    float argoRange;

    [SerializeField]
    float moveSpeed;

    Rigidbody2D rb2d;

    public AudioClip SoEnemigo;
    private Puntos ManagerPuntos;
    public int Puntos;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        
        ManagerPuntos = FindObjectOfType<Puntos>();
    }

    // Update is called once per frame
    void Update()
    {
        float distToPlayer = Vector2.Distance(transform.position, player.position);

        if (distToPlayer < argoRange)
        {
            ChasePlayer();
        }
        else
        {
            StopChasingPlayer();
        }
    }
    
    void ChasePlayer()
    {

        Vector2 direccion = player.position - transform.position;
        rb2d.velocity = direccion.normalized * moveSpeed;

        if (transform.position.x < player.position.x)
        {
            
            transform.localScale = new Vector2(0.3f, 0.3f);

        }
        else
        {
           
            transform.localScale = new Vector2(-0.3f, 0.3f);
        }


    }

    void StopChasingPlayer()
    {
        rb2d.velocity = new Vector2(0, 0);
    }

    private void OnTriggerEnter2D(Collider2D Colicion)
    {
        if (Colicion.CompareTag("OndaPES"))
        {
            SoundManager.PlaySfx(SoEnemigo);
            ManagerPuntos.ValPunTos += Puntos;
            ManagerPuntos.ValEnemigo++;
        }
    }
}
