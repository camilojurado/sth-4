﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Golpeado : MonoBehaviour
{
    
    
    Animator AnimacionComp;

    public bool damage_;
    public int empuje;

    public Rigidbody2D _rigidbody2D;
    public AudioClip golpeado;

    private Puntos ManagerAnillos;
    public int GameOver;

    public Transparencia Simbolo;
    
    // Start is called before the first frame update
    void Start()
    {
        AnimacionComp = GetComponent<Animator>();
        ManagerAnillos = FindObjectOfType<Puntos>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Damage()
    {
        if (damage_)
        {
            //transform.Translate(Vector3.right * empuje * Time.deltaTime, Space.World);
            _rigidbody2D.AddForce(transform.up * empuje);
        }
    }

    public void Finish_Damage()
    {
        damage_ = false;
    }
    
    private void OnTriggerEnter2D (Collider2D Colicion)
    {
        
        
        if (Colicion.CompareTag("Enemigo"))
        {
            if (Simbolo.modo != Transparencia.Modo.Nothing)
            {
                return;
            }
            
            damage_ = true;
            Damage();
            
            SoundManager.PlaySfx(golpeado);
            AnimacionComp.SetTrigger("SilverDanado");
            
            if (ManagerAnillos.ValAnillos == 0)
            {
                SceneManager.LoadScene(GameOver);
            }

            ManagerAnillos.ValAnillos = 0;
            
            
            Finish_Damage();
            
            Debug.Log("Golpeado");
            
            
        }

        if (Colicion.CompareTag(("PUAS")))
        {
            Damage();
            SoundManager.PlaySfx(golpeado);
            
            if (ManagerAnillos.ValAnillos == 0)
            {
                SceneManager.LoadScene(GameOver);
            }
            
            ManagerAnillos.ValAnillos = 0;
            
            AnimacionComp.SetTrigger("SilverDanado");
            Finish_Damage();
            
            
        }
        
        if (Colicion.CompareTag(("Acido")))
        {
            Damage();
            SoundManager.PlaySfx(golpeado);
            
            
            
            ManagerAnillos.ValAnillos = 0;
            
            AnimacionComp.SetTrigger("SilverDanado");
            Finish_Damage();
            if (ManagerAnillos.ValAnillos == 0)
            {
                SceneManager.LoadScene(GameOver);
            }
            
        }
        
        
        if (Colicion.CompareTag(("ProyectilBug")))
        {
            
            if (Simbolo.modo != Transparencia.Modo.Nothing)
            {
                return;
                
            }
            
            damage_ = true;
            
            Damage();
            SoundManager.PlaySfx(golpeado);
            
            if (ManagerAnillos.ValAnillos == 0)
            {
                SceneManager.LoadScene(GameOver);
            }
            
            ManagerAnillos.ValAnillos = 0;
            
            AnimacionComp.SetTrigger("SilverDanado");
            Finish_Damage();
            
            
        }



    }
    
}
