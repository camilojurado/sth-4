﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SilverCorriendo : MonoBehaviour
{

    [SerializeField]

    float Vel = 3.0f;


    RefAnimator silver;

    // Start is called before the first frame update
    void Start()
    {
        silver = GetComponent<RefAnimator>();
    }

    // Update is called once per frame
    void Update()
    {
        Correr();   
    }

    void Correr()
    {
        //if (Input.GetKey("d"))

        float Horizontal = Input.GetAxis("Horizontal");

        if (Horizontal != 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = Horizontal < 0;
            transform.Translate(Vector2.right * Time.deltaTime * Vel * Horizontal);
        }
        silver.Corriendo(Horizontal != 0);
        //silver.SetBool("SilverCorriendo", Horizontal != 0);
        

       


    }

}
