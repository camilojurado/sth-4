using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SilverStats : Singleton<SilverStats>
{
    private Animator AnimacionComp;
    
    
    void Start()
    {
        AnimacionComp = GetComponent<Animator>();
    }

    
    void Update()
    {
        
    }

    public Animator GetAnimator()
    {
        return AnimacionComp;
    }
}
