using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoH : MonoBehaviour
{

    [SerializeField] 
    float Vel = 3.0f;
    
    
    Animator AnimationComp;
    
    
    
    void Start()
    {
        AnimationComp = GetComponent<Animator>();
    }

    
    void Update()
    {
        MoveHorizontal();
    }

    public void MoveHorizontal()
    {
        float Horizontal = Input.GetAxis("Horizontal");

        if (Horizontal != 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = Horizontal < 0;
            transform.Translate(Vector3.right * Time.deltaTime * Vel * Horizontal);
            AnimationComp.SetBool("MOVI", true);
        }
        else
        {
            AnimationComp.SetBool("MOVI", false);
        }
        
    }

    
    
}
