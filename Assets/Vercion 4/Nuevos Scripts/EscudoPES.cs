using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscudoPES : MonoBehaviour
{
    [SerializeField] private Transform PuntoOndaArriba;
    
    [SerializeField] private Transform PuntoOndaEste;
    
    [SerializeField] private Transform PuntoOndaOeste;


    [SerializeField] private GameObject OndaPES;
    
    
    private Animator AnimacionComp;
    
    void Start()
    {
        AnimacionComp = GetComponent<Animator>();
    }

    
    void Update()
    {
        OndaEscudo();
    }

    public void OndaEscudo()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            TranparenciaBARRA.Instance.estado = TranparenciaBARRA.Estado.Show;
            AnimacionComp.SetTrigger("PESescudo");

            GameObject WAVEArriba = Instantiate(OndaPES, PuntoOndaArriba.transform.position,
                PuntoOndaArriba.transform.rotation, transform);
            
            
            GameObject WAVEEste = Instantiate(OndaPES, PuntoOndaEste.transform.position,
                PuntoOndaEste.transform.rotation, transform);
            
            
            GameObject WAVEOeste = Instantiate(OndaPES, PuntoOndaOeste.transform.position,
                PuntoOndaOeste.transform.rotation, transform);
        }
        
        if (Input.GetKeyUp(KeyCode.G))
        {
            TranparenciaBARRA.Instance.estado = TranparenciaBARRA.Estado.Hide;
        }
    }
}
