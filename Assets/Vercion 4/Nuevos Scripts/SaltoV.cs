using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoV : MonoBehaviour
{
    [SerializeField] 
    float Salto = 10f;

    public float DistanciaRay;
    
    public LayerMask Piso;
    public bool EnelAire = false;
    
    Animator AnimationComp;
    
    
    void Start()
    {
        AnimationComp = GetComponent<Animator>();
    }

    
    void Update()
    {
        SaltoVertical();
        EnColicion();
    }

    public void SaltoVertical()
    {
        if (Input.GetKeyDown(KeyCode.Space) && EnColicion())
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * Salto, ForceMode2D.Impulse);
            AnimationComp.SetTrigger("ElSalto");
            AnimationComp.SetBool("SilverCayendo", true);
        }
        else
        {
            AnimationComp.ResetTrigger("ElSalto");
            AnimationComp.SetBool("SilverCayendo", false);
        }
    }


    public bool EnColicion()
    {
        if (Physics2D.Raycast(transform.position, Vector2.down, DistanciaRay, Piso))
        {
            AnimationComp.SetBool("SilverParado", true);
            EnelAire = false;
            return true;
        }
        else
        {
            AnimationComp.SetBool("SilverParado", false);
            EnelAire = true;
            return false;
        }
    }

    // Esto ayuda a ver la distancia del Salto
    void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position,Vector2.down * DistanciaRay);
        
        
    }
    
    
    
    
    
    
}
