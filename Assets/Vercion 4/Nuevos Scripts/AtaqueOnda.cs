using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AtaqueOnda : MonoBehaviour
{
    [SerializeField] private Transform PuntoPES;

    [SerializeField] private GameObject Onda;
    
    private Animator AnimacionComp;
    
    void Start()
    {
        AnimacionComp = GetComponent<Animator>();
    }

    
    void Update()
    {
        PESBasico();
    }


    public void PESBasico()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            TranparenciaBARRA.Instance.estado = TranparenciaBARRA.Estado.Show;
            AnimacionComp.SetTrigger("AtaqueBasico");
            GameObject Wave = Instantiate(Onda, PuntoPES.transform.position, PuntoPES.transform.rotation, transform);
            
        }

        if (Input.GetKeyUp(KeyCode.F))
        {
            TranparenciaBARRA.Instance.estado = TranparenciaBARRA.Estado.Hide;
        }
    }
}
