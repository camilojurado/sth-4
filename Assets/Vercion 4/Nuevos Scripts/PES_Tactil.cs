using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PES_Tactil : MonoBehaviour
{
    Vector2 difference = Vector2.zero;
    
    Animator AnimationComp;

    void Start()
    {
        AnimationComp = GetComponent<Animator>();
    }

    void Update()
    {
        Tactil();
    }
    
    private void OnMouseDown()
    {
        
        
        difference = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - (Vector2)transform.position;
    }

    private void OnMouseDrag()
    {
        if (TranparenciaBARRA.Instance.EstadoNada() == true)
        {
            return;
        }
        
        transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - difference;
    }

    public void Tactil()
    {
        
        
        
        if (Input.GetMouseButtonDown(0))
        {
            TranparenciaBARRA.Instance.estado = TranparenciaBARRA.Estado.Show;
            SilverStats.Instance.GetAnimator().SetBool("Sostener", true);
        }

        if (Input.GetMouseButtonUp(0))
        {
            TranparenciaBARRA.Instance.estado = TranparenciaBARRA.Estado.Hide;
            SilverStats.Instance.GetAnimator().SetBool("Sostener", false);
        }
        
    }
    
    
}
