using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgareDeObjetos : MonoBehaviour
{
    Vector2 Diferencia = Vector2.zero;

    private void OnMouseDown()
    {
        Diferencia = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - (Vector2)transform.position;
    }

    private void OnMouseDrag()
    {
        transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - Diferencia;
    }

}
