using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranparenciaBARRA : Singleton<TranparenciaBARRA>
{
    [Range(0, 1)]
    public float Transp = 0, VeloTrans = 1;

    public Image barrita_Movimiento;
    
    

    public enum Estado
    {
        Show = 0,
        Hide = 1,
        Nada = -1,

    };

    public Estado estado;
    
    void Start()
    {
        estado = Estado.Nada;
        barrita_Movimiento = GetComponent<Image>();
    }

    
    void Update()
    { 
        
        
        if (estado.Equals(Estado.Show))
        {
            if (Transp <= 0)
                estado = Estado.Nada;

            Transp -= Time.deltaTime;
            barrita_Movimiento.fillAmount = Transp;
            

        }

        if (estado.Equals(Estado.Hide))
        {
            if (Transp >= 1)
                estado = Estado.Nada;

            Transp += Time.deltaTime / 2;
            barrita_Movimiento.fillAmount = Transp;
            
        }
        
        
        
    }


    public bool EstadoNada()
    {
        return estado == Estado.Nada;
    }
}
