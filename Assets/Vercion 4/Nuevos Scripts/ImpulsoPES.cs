using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpulsoPES : MonoBehaviour
{
    private Animator AnimacionComp;
    
    public float DistanciaRay;
    
    public LayerMask Piso;
    
    public bool EnelAire = false;
    
    void Start()
    {
        AnimacionComp = GetComponent<Animator>();
    }

    
    void Update()
    {
        PESdash();
    }

    public void PESdash()
    {
        if (Input.GetKeyDown(KeyCode.H) && !EnColicion())
        {
            TranparenciaBARRA.Instance.estado = TranparenciaBARRA.Estado.Show;
            AnimacionComp.SetTrigger("SilverImpulso");
            Vector2 dir = GetComponent<SpriteRenderer>().flipX ? -transform.right : transform.right;
            gameObject.GetComponent<Rigidbody2D>().AddForce(dir * 30, ForceMode2D.Impulse);
        }

        if (Input.GetKeyUp(KeyCode.H))
        {
            TranparenciaBARRA.Instance.estado = TranparenciaBARRA.Estado.Hide;
        }
    }


    public bool EnColicion()
    {
        if (Physics2D.Raycast(transform.position, Vector2.down, DistanciaRay, Piso))
        {
            AnimacionComp.SetBool("SilverParado", true);
            EnelAire = false;
            return true;
        }
        else
        {
            AnimacionComp.SetBool("SilverParado", false);
            EnelAire = true;
            return false;
        }

    }
    
    void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position,Vector2.down * DistanciaRay);
        
        
    }
}
