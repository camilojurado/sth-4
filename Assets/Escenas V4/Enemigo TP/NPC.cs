using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public AudioClip SoEnemigo;
    private Puntos ManagerPuntos;
    public int Puntos;
    
    // Start is called before the first frame update
    void Start()
    {
        ManagerPuntos = FindObjectOfType<Puntos>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter2D(Collider2D Colicion)
    {
        if (Colicion.CompareTag("OndaPES"))
        {
            SoundManager.PlaySfx(SoEnemigo);
            ManagerPuntos.ValPunTos += Puntos;
            ManagerPuntos.ValEnemigo++;
        }
    }
}
