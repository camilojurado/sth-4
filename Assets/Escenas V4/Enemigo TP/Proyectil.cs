using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{
    private GameObject Player;

    public GameObject proyectil;

    public float force;

    public float TimerDelay;

    private float TimerDelayAux;

    public GameObject PointSpwan;
    
    
    private Animator AnimacionComp;

    // Start is called before the first frame update
    void Start()
    {
        TimerDelayAux = TimerDelay;
        Player = GameObject.FindGameObjectWithTag("Player");
        AnimacionComp = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(gameObject.transform.position, Player.transform.position) < 3)
        {
            AnimacionComp.SetBool("Disparo", true);
            TimerDelay -= Time.deltaTime;
            if(TimerDelay<= 0) 
            {
                TimerDelay = TimerDelayAux;
                Vector3 direccion = Player.transform.position - gameObject.transform.position;


                GameObject NewProyectil = Instantiate(proyectil,PointSpwan.transform.position, Quaternion.identity);
                NewProyectil.GetComponent<Rigidbody2D>().AddForce((direccion.normalized + Vector3.up) * force, ForceMode2D.Impulse);
                
                
                
            }
            

            
        }
        else
        {
            AnimacionComp.SetBool("Disparo", false);
        }
        

    }

}
