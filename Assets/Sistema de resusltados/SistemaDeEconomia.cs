using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class SistemaDeEconomia
{
    public int Anillos;

    public const string KeyCurrencies = "77";

    

    public void IncrementarCurrencies(int Valor)
    {
        var recuest = new AddUserVirtualCurrencyRequest()
        {
            VirtualCurrency = KeyCurrencies,
            Amount = Valor
        };

        PlayFabClientAPI.AddUserVirtualCurrency(recuest, OnCurrencies, OnError);
    }

    public void OnError(PlayFabError error)
    {
        Debug.LogError(error.ErrorMessage);
    }

    public void OnCurrencies(ModifyUserVirtualCurrencyResult result)
    {
        Debug.Log("Se Actualizo");
    }
    
    

    
}
