using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IniciacerPLAYFAB : MonoBehaviour
{
    [SerializeField] private InputField _usernameInputField;
    
    [SerializeField] private InputField _passwordInputField;
    
    
    
    public void LogIn()
    {
        LoginWithPlayFabRequest request = new LoginWithPlayFabRequest()
        {
            Username = _usernameInputField.text.Replace(" ", ""),
            Password = _passwordInputField.text,
            TitleId = PlayFabSettings.TitleId,
        };
        PlayFabClientAPI.LoginWithPlayFab(request, OnResultCallback, OnErrorCallback);

    }
    
    
    private void OnErrorCallback(PlayFabError error)
    {
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
        string message = "";
        if (error.ErrorDetails != null)
        {
            foreach (var i in error.ErrorDetails)
            {
                foreach (var j in i.Value)
                {
                    //message = message + j;
                    if (j.Contains("Username contains invalid characters"))
                    {
                        message = message + "El Usuario contiene caracteres inválidos" + "\n";
                    }
                    else if (j.Contains("Username must be between 3 and 20 characters"))
                    {
                        message = message + "El Usuario debe tener entre 3 y 20 caracteres" + "\n";
                    }
                    else if (j.Contains("User name already exists"))
                    {
                        message = message + "El Usuario no está disponible" + "\n";
                    }
                    else if (j.Contains("Email address is not valid"))
                    {
                        message = message + "El Correo no es válido" + "\n";
                    }
                    else if (j.Contains("Email address already exists"))
                    {
                        message = message + "El Correo no está disponible" + "\n";
                    }
                    else if (j.Contains("Password must be between 6 and 100 characters"))
                    {
                        message = message + "La contraseña debe tener entre 6 y 100 caracteres" + "\n";
                    }
                    else if (j.Contains("User not found"))
                    {
                        message = message + "Usuario no encontrado" + "\n";
                    }
                    Debug.Log("********" + j);
                }
            }
        }
        if (message == "")
        {
            message = "Verifica que todos tus datos esten debidamente llenados e intenta de nuevo";
        }
        
        NotificationPanel.Instance.ShowNotification(message);
        

        //SceneController._instance.CloseStaticLoadingPanel();
        
        
        
    }

    private void OnResultCallback(LoginResult obj)
    {
        PlayerPrefs.SetString("PlayFabId", obj.PlayFabId);
        //SceneManager.LoadScene("MenuPrincipal");
        Debug.Log("Inicio de secion correcto");
        ObtencionDeUsuarioPLAYFABs Datos = new ObtencionDeUsuarioPLAYFABs();
        
        Datos.GetPLayerDATA(PlayerPrefs.GetString("PlayFabId"));

    }

    public void ActivarPanel()
    {
        //En El Avtive Indica si esta Activo Esta falso y si esta desactivado se pone en true
        gameObject.SetActive(!gameObject.activeSelf);
    }
    
}
