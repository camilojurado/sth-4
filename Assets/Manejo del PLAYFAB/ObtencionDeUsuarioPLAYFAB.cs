using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ObtencionDeUsuarioPLAYFABs
{
    public string DisplayName;

    public int Anillos;
    
    
    
    public void GetPLayerDATA(string PLayFabID)
    {
        var Recuest = new GetAccountInfoRequest()
        {
            PlayFabId = PLayFabID,
        };
        
        PlayFabClientAPI.GetAccountInfo(Recuest,DataSucsses,DataError);
    }


    public void DataSucsses(GetAccountInfoResult Resultados)
    {

        DisplayName = Resultados.AccountInfo.Username;
            

        // En el inventory Se accede al VirtualCurrency y A los objeto o Items que el usuario ha Comprado
        var Recuest = new GetUserInventoryRequest();
        PlayFabClientAPI.GetUserInventory(Recuest, InventorySucsses, DataError);
        
    }

    public void InventorySucsses(GetUserInventoryResult result)
    {
        //Con el code de Currencies de PlayFab, Validamos si es que tiene el jugador el VirtualCurrency
        if (result.VirtualCurrency.ContainsKey("77"))
        {
            Anillos = result.VirtualCurrency["77"];
        }
        PerfilInicio.Instance.SetPerfil(DisplayName,Anillos);
    }
    
    public void DataError(PlayFabError error)
    {
        Debug.Log("Error Message" + error.ErrorMessage);
    }
    
}
