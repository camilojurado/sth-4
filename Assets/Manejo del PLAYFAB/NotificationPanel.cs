using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationPanel : MonoBehaviour
{

    public static NotificationPanel Instance;
    [SerializeField] private GameObject _panel;

    [SerializeField] private Text _notificationText;
    
    
    void Awake()
    {
        Instance = this;
    }
    

    public void ShowNotification(string message)
    {
        _panel.SetActive(true);
        _notificationText.text = message;
    }
    
}
