using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class PerfilInicio : MonoBehaviour
{
    public static PerfilInicio Instance;
    
    public Text NombreUsuario;
    public Text NumeroDeAnillos;

    public Button BontonEntrarJuego;


    void Awake()
    {
        // Se vuelve todo en una Clase Estatica, Todo lo que tenga el mismo Script Funciona como uno solo
        //Sin Tener que hacerle una Referencia
        Instance = this;
    }

    public void SetPerfil(string usuario, int LosNumeroDeAnillos)
    {
        NombreUsuario.text = usuario;

        NumeroDeAnillos.text = LosNumeroDeAnillos.ToString();

        BontonEntrarJuego.interactable = true;
    }
    
}
