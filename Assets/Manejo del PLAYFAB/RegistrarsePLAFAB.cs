using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;

public class RegistrarsePLAFAB : MonoBehaviour
{
    [SerializeField] private InputField _usernameInputField;
    [SerializeField] private InputField _passwordInputField;
    
    
    public void OnRegiter()
    {
        RegisterPlayFabUserRequest recuest = new RegisterPlayFabUserRequest()
        {
            DisplayName = _usernameInputField.text,
            Username = _usernameInputField.text,
            Password = _passwordInputField.text,
            TitleId = PlayFabSettings.TitleId,
            RequireBothUsernameAndEmail = false,

        };
        PlayFabClientAPI.RegisterPlayFabUser(recuest, OnResultCallback, OnErrorCallback);
        //PlayFabClientAPI.UpdateUserData();
    }
    
    private void OnErrorCallback(PlayFabError error)
    {
        
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
        string message = "";
        if (error.ErrorDetails != null)
        {
            foreach (var i in error.ErrorDetails)
            {
                foreach (var j in i.Value)
                {
                    //message = message + j;
                    if (j.Contains("Username contains invalid characters"))
                    {
                        message = message + "El Usuario contiene caracteres inválidos" + "\n";
                    }
                    else if (j.Contains("Username must be between 3 and 20 characters"))
                    {
                        message = message + "El Usuario debe tener entre 3 y 20 caracteres" + "\n";
                    }
                    else if (j.Contains("User name already exists"))
                    {
                        message = message + "El Usuario no está disponible" + "\n";
                    }
                    else if (j.Contains("Email address is not valid"))
                    {
                        message = message + "El Correo no es válido" + "\n";
                    }
                    else if (j.Contains("Email address already exists"))
                    {
                        message = message + "El Correo no está disponible" + "\n";
                    }
                    else if (j.Contains("Password must be between 6 and 100 characters"))
                    {
                        message = message + "La contraseña debe tener entre 6 y 100 caracteres" + "\n";
                    }
                    else if (j.Contains("User not found"))
                    {
                        message = message + "Usuario no encontrado" + "\n";
                    }
                    Debug.Log("********" + j);
                }
            }
        }
        if (message == "")
        {
            message = "Verifica que todos tus datos esten debidamente llenados e intenta de nuevo";
        }
        
        NotificationPanel.Instance.ShowNotification(message);
        

    }

    private void OnResultCallback(RegisterPlayFabUserResult result)
    {
        Debug.Log("registrado");
        
        //Este Script Es para obtener solo los Datos del Jugador y ponerlo en el perfil
        ObtencionDeUsuarioPLAYFABs Datos = new ObtencionDeUsuarioPLAYFABs();
        
        Datos.GetPLayerDATA(PlayerPrefs.GetString("PlayFabId"));
    }
    
    
    
}
