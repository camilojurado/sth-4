using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnilloMETA : MonoBehaviour
{
    public GameObject PantallaDeResultados;

    public Text Puntuacion;
    public Text Anillos;
    public Text Tiempo;

    public string EscenaNombre;


    private Animator AnimacionComp;
    public AudioClip KChin;
    
    // Start is called before the first frame update
    void Start()
    {
        AnimacionComp = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void OnTriggerEnter2D(Collider2D collision)
    {
        //Si ya esta activado una vez no podra activarse otra vez
        if (collision.CompareTag("Player") && PantallaDeResultados.activeSelf == false)
        {

            Reloj reloj = FindObjectOfType<Reloj>();

            AnimacionComp.SetTrigger("EscalaAnillo");
            SoundManager.PlaySfx(KChin);
            
            Debug.Log("LLego a la Meta");

            PantallaDeResultados.SetActive(true);
            //SistemaDeEconomia sistemaDeEconomia = new SistemaDeEconomia();
            //sistemaDeEconomia.GuardarAnillos(FindObjectOfType<Puntos>().ValAnillos);

            Puntuacion.text = Puntos.instance.ValPunTos.ToString();
            Anillos.text = Puntos.instance.ValAnillos.ToString();
            reloj.estaPausado = true;
            Tiempo.text = reloj.GetTimeText();
            StartCoroutine(TiempoDeCambioDeEscena());

        }
        
    }

    IEnumerator TiempoDeCambioDeEscena()
    {
        yield return new WaitForSeconds(10);
        CambioDeEscena cambioDeEscena = new CambioDeEscena();
        
        cambioDeEscena.CambiarEscenaPorNombre(EscenaNombre);
        
    }
    
    
}
